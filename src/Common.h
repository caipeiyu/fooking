#pragma once

#include <string>

#define _DEBUG

#define NS_NAME				fooking
#define NS_BEGIN			namespace NS_NAME{
#define NS_END				}
#define NS_USING			using namespace NS_NAME

#define zmalloc				malloc
#define zfree				free
#define zrealloc			realloc

#define PROTOCOL_RAW		1
#define PROTOCOL_FASTCGI	2

#define SOCKET_NONE			0
#define SOCKET_TCP			1
#define SOCKET_UNIX			2

#define SID_LENGTH			20
#define SID_FULL_LEN		21

/* Anti-warning macro... */
#define NOTUSED(V) 			((void) V)

typedef unsigned char	uint_8;
typedef unsigned short	uint_16;

//socket类型
typedef struct{
	int		type;
	union{
		struct{
			short		tcp_port;
			char		tcp_host[58];
		};
		struct{
			char		unix_name[60];
		};
	};
}SocketOption;

inline short readNetInt16(const char *ptr)
{
	short n = ((short)ptr[0] << 8) | ptr[1];
	return n;
}

inline int readNetInt32(const char *ptr)
{
	int n = ((int)ptr[0] << 24) | ((int)ptr[1] << 16) | ((int)ptr[2] << 8) | ptr[3];
	return n;
}

inline void writeNetInt16(char *ptr, short n)
{
	ptr[0] = (n >> 8) & 0xFF;
	ptr[1] = n & 0xFF;
}

inline void writeNetInt32(char *ptr, int n)
{
	ptr[0] = (n >> 24) & 0xFF;
	ptr[1] = (n >> 16) & 0xFF;
	ptr[2] = (n >> 8) & 0xFF;
	ptr[3] = n & 0xFF;
}