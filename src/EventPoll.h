#pragma once

NS_BEGIN

class EventLoop;

//poll interface
class IEventPoll{
public:
	IEventPoll(EventLoop *loop):
		pLoop(loop){}
	virtual ~IEventPoll(){}
public:
	virtual int 		add(int fd, int mask) = 0;
	virtual int 		del(int fd, int mask) = 0;
	virtual int 		poll(struct timeval *tvp = NULL) = 0;
protected:
	EventLoop*			pLoop;
};

//epoll
class EventEPoll:
	public IEventPoll
{
public:
	EventEPoll(EventLoop *loop);
	~EventEPoll();
public:
	int 				add(int fd, int mask);
	int 				del(int fd, int mask);
	int 				poll(struct timeval *tvp = NULL);
private:
	int					epfd;
	struct epoll_event*	events;
};

NS_END